import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Form from "./Form";

function App() {
  return (
    <div className="App">
      <img src={logo} className="App-logo" alt="logo" />
      <Form />
    </div>
  );
}

export default App;
